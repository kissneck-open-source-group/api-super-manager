/*
 Navicat Premium Data Transfer

 Source Server         : 可待云本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : localhost:3306
 Source Schema         : openapi_demo

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 23/11/2021 17:05:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_app
-- ----------------------------
DROP TABLE IF EXISTS `admin_app`;
CREATE TABLE `admin_app`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '应用id',
  `app_secret` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '应用密码',
  `app_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '应用名称',
  `app_status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '应用状态：0表示禁用，1表示启用',
  `app_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '应用说明',
  `app_api` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '当前应用允许请求的全部API接口',
  `app_group` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'default' COMMENT '当前应用所属的应用组唯一标识',
  `app_add_time` int(11) NOT NULL DEFAULT 0 COMMENT '应用创建时间',
  `app_api_show` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '前台样式显示所需数据格式',
  `app_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '应用域名',
  `app_git_path` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '应用仓库git ssh克隆地址',
  `orgin_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'API平台ID唯一编码',
  `copy_path` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '副本项目文件路径',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `app_id`(`app_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'appId和appSecret表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_app
-- ----------------------------
INSERT INTO `admin_app` VALUES (1, '59321067', 'EkyeBWsYpqngBhIZcBuEChpyfIVaDqLl', '测试0001', 1, '', '6195b8ae1f017,6195d02088d07,6195d0e197c8f,619772571939e', '6195b7acbb478', 1637202082, '{\"default\":[\"6195b8ae1f017\",\"619772571939e\"],\"6195bf9c6c009\":[\"6195d02088d07\",\"6195d0e197c8f\"]}', 'https://apism.kissneck.com/', '0.0.0.1', '202111181021848181361', NULL);
INSERT INTO `admin_app` VALUES (2, '60853876', 'xzKBUzxlAhsITbDGweluRlEUpUgHWkcW', '测试0002', 1, '', '', '6195cfb8824ad', 1637208056, '{\"6195bf9c6c009\":[]}', 'https://apism.kissneck.com//test2/', '1', '202111181200673939027', NULL);

-- ----------------------------
-- Table structure for admin_app_group
-- ----------------------------
DROP TABLE IF EXISTS `admin_app_group`;
CREATE TABLE `admin_app_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组名称',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '组说明',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '组状态：0表示禁用，1表示启用',
  `hash` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '应用组，目前只做管理使用，没有实际权限控制' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_app_group
-- ----------------------------
INSERT INTO `admin_app_group` VALUES (1, '测试01', '第一版测试', 1, '6195b7acbb478');
INSERT INTO `admin_app_group` VALUES (2, '测试0102', '', 1, '6195cfb1cc8db');
INSERT INTO `admin_app_group` VALUES (3, '测试0201', '', 1, '6195cfb8824ad');

-- ----------------------------
-- Table structure for admin_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `admin_auth_group`;
CREATE TABLE `admin_auth_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组名称',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '组描述',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '组状态：为1正常，为0禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_auth_group
-- ----------------------------
INSERT INTO `admin_auth_group` VALUES (1, '测试应用权限', '', 1);

-- ----------------------------
-- Table structure for admin_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `admin_auth_group_access`;
CREATE TABLE `admin_auth_group_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `group_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和组的对应关系' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_auth_group_access
-- ----------------------------
INSERT INTO `admin_auth_group_access` VALUES (1, 2, '');
INSERT INTO `admin_auth_group_access` VALUES (2, 3, '');
INSERT INTO `admin_auth_group_access` VALUES (3, 4, '2');
INSERT INTO `admin_auth_group_access` VALUES (4, 5, '2');
INSERT INTO `admin_auth_group_access` VALUES (5, 8, '2');
INSERT INTO `admin_auth_group_access` VALUES (6, 9, '2');
INSERT INTO `admin_auth_group_access` VALUES (7, 10, '2');
INSERT INTO `admin_auth_group_access` VALUES (9, 12, '2');
INSERT INTO `admin_auth_group_access` VALUES (10, 13, '2');
INSERT INTO `admin_auth_group_access` VALUES (11, 14, '2');
INSERT INTO `admin_auth_group_access` VALUES (12, 15, '2');
INSERT INTO `admin_auth_group_access` VALUES (13, 16, '2');
INSERT INTO `admin_auth_group_access` VALUES (14, 17, '2');
INSERT INTO `admin_auth_group_access` VALUES (15, 18, '2');
INSERT INTO `admin_auth_group_access` VALUES (16, 19, '2');

-- ----------------------------
-- Table structure for admin_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `admin_auth_rule`;
CREATE TABLE `admin_auth_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '权限所属组的ID',
  `auth` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '权限数值',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态：为1正常，为0禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限细节' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_auth_rule
-- ----------------------------
INSERT INTO `admin_auth_rule` VALUES (1, 'admin/Login/index', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (2, 'admin/Login/logout', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (3, 'admin/Index/upload', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (4, 'admin/User/own', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (5, 'admin/Login/getUserInfo', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (6, 'admin/Login/getAccessMenu', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (7, 'admin/App/changeStatus', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (8, 'admin/App/getAppInfo', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (9, 'admin/App/add', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (10, 'admin/App/edit', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (11, 'admin/App/del', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (12, 'admin/App/index', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (13, 'admin/App/refreshAppSecret', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (14, 'admin/InterfaceList/group', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (21, 'admin/InterfaceList/changeStatus', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (22, 'admin/InterfaceList/getHash', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (23, 'admin/InterfaceList/add', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (24, 'admin/InterfaceList/edit', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (25, 'admin/InterfaceList/del', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (26, 'admin/Fields/add', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (27, 'admin/Fields/upload', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (28, 'admin/Fields/edit', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (29, 'admin/Fields/del', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (30, 'admin/InterfaceList/index', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (31, 'admin/InterfaceList/refresh', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (32, 'admin/Fields/request', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (33, 'admin/Fields/response', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (34, 'admin/InterfaceGroup/add', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (35, 'admin/InterfaceGroup/edit', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (36, 'admin/InterfaceGroup/del', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (37, 'admin/InterfaceGroup/getAll', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (38, 'admin/InterfaceGroup/changeStatus', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (39, 'admin/InterfaceGroup/index', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (46, 'admin/AppGroup/getAll', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (47, 'admin/AppGroup/index', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (48, 'admin/InterfaceList/createFunc', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (50, 'admin/SaasDoc/del', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (51, 'admin/SaasDoc/changeStatus', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (55, 'admin/CustomDataBase/designTableList', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (56, 'admin/CustomDataBase/addDesignTable', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (57, 'admin/CustomDataBase/addDesignColumn', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (58, 'admin/CustomDataBase/addDesignColumn', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (59, 'admin/SaasDoc/index', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (60, 'admin/SaasDoc/addFields', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (61, 'admin/SaasDoc/upload', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (62, 'admin/SaasDoc/response', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (63, 'admin/SaasDoc/editField', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (64, 'admin/SaasDoc/group', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (65, 'admin/SaasDoc/add', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (66, 'admin/SaasDoc/saasGroupIndex', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (67, 'admin/SaasDoc/saasChangeStatus', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (68, 'admin/SaasDoc/saasGroupAdd', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (69, 'admin/SaasDoc/saasGroupEdit', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (70, 'admin/SaasDoc/saasGroupDel', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (71, 'admin/SaasDoc/edit', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (72, 'admin/SaasDoc/responseApi', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (73, 'admin/SaasDoc/delField', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (76, 'admin/CustomDataBase/designColumnList', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (77, 'admin/CustomDataBase/delDesignColumn', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (78, 'admin/CustomDataBase/editDesignTable', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (79, 'admin/CustomDataBase/delDesignTable', 2, 0, 1);
INSERT INTO `admin_auth_rule` VALUES (80, 'admin/CustomDataBase/editDesignColumn', 2, 0, 1);

-- ----------------------------
-- Table structure for admin_fields
-- ----------------------------
DROP TABLE IF EXISTS `admin_fields`;
CREATE TABLE `admin_fields`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '字段名称',
  `hash` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限所属组的ID',
  `data_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '数据类型，来源于DataType类库',
  `default` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '默认值',
  `is_must` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否必须 0为不必须，1为必须',
  `range` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '范围，Json字符串，根据数据类型有不一样的含义',
  `info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '字段说明',
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '字段用处：0为request，1为response',
  `show_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'wiki显示用字段',
  `orgin_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'API平台ID唯一编码',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `hash`(`hash`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 191 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用于保存各个API的字段规则' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_fields
-- ----------------------------
INSERT INTO `admin_fields` VALUES (184, 'data', '6195b8ae1f017', 9, '', 1, '', '', 1, 'data', '202111181143987766168');
INSERT INTO `admin_fields` VALUES (185, 'feedId', '6195b8ae1f017', 1, '', 1, '', '', 1, 'data{}feedId', '202111181143987775088');
INSERT INTO `admin_fields` VALUES (186, 'feedType', '6195b8ae1f017', 2, '', 1, '', '', 1, 'data{}feedType', '202111181143987779379');
INSERT INTO `admin_fields` VALUES (187, 'marketplaceIds', '6195b8ae1f017', 3, '', 1, '', '', 1, 'data{}marketplaceIds', '202111181143987781121');
INSERT INTO `admin_fields` VALUES (188, 'resultFeedDocumentId', '6195b8ae1f017', 2, '', 1, '', '', 1, 'data{}resultFeedDocumentId', '202111181143987784167');
INSERT INTO `admin_fields` VALUES (189, 'password', '6195b8ae1f017', 2, '', 1, '', '用户密码', 0, 'password', '202111181146803038122');
INSERT INTO `admin_fields` VALUES (190, 'username1', '6195b8ae1f017', 2, '', 1, '', '用户账号', 0, 'username1', '202111181158193626919');

-- ----------------------------
-- Table structure for admin_group
-- ----------------------------
DROP TABLE IF EXISTS `admin_group`;
CREATE TABLE `admin_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组名称',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '组说明',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态：为1正常，为0禁用',
  `hash` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组标识',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  `image` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分组封面图',
  `hot` int(11) NOT NULL DEFAULT 0 COMMENT '分组热度',
  `app_hash` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '对应admin_app表中的app_group',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '接口组管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_group
-- ----------------------------
INSERT INTO `admin_group` VALUES (1, '测试接口组no1', '', 1, '6195bf9c6c009', 1637203880, 1637314933, '', 35, '6195b7acbb478');

-- ----------------------------
-- Table structure for admin_list
-- ----------------------------
DROP TABLE IF EXISTS `admin_list`;
CREATE TABLE `admin_list`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_class` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'api索引，保存了类和方法',
  `hash` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'api唯一标识',
  `access_token` tinyint(4) NOT NULL DEFAULT 1 COMMENT '认证方式 1：复杂认证，0：简易认证',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'API状态：0表示禁用，1表示启用',
  `method` tinyint(4) NOT NULL DEFAULT 2 COMMENT '请求方式0：不限1：Post，2：Get 3:PUT 4:DELETE',
  `info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'api中文说明',
  `des` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '【A】接口说明',
  `is_test` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否是测试模式：0:生产模式，1：测试模式',
  `return_str` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回数据示例',
  `group_hash` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'default' COMMENT '当前接口所属的接口分组',
  `hash_type` tinyint(4) NOT NULL DEFAULT 2 COMMENT '是否采用hash映射， 1：普通模式 2：加密模式',
  `app_group_id` int(10) NOT NULL COMMENT '当前所属应用接口模块id',
  `app_group_hash` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '当前接口所属的app分组',
  `api_type` tinyint(1) NULL DEFAULT 1 COMMENT '接口的类型 1api接口 2公共外部访问接口',
  `create_flag` tinyint(1) NULL DEFAULT 0 COMMENT '是否创建过该域名代码 0未创建 1已创建',
  `orgin_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'API平台ID唯一编码',
  `router_type` tinyint(1) NULL DEFAULT 0 COMMENT '路由类型  0默认路由  1资源路由',
  `md_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'md文件地址',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `hash`(`hash`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用于维护接口信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_list
-- ----------------------------
INSERT INTO `admin_list` VALUES (1, 'user/login', '6195b8ae1f017', 0, 1, 2, '登陆', '登陆页面', 0, '{\"code\":200,\"message\":\"\\u83b7\\u53d6\\u6210\\u529f\",\"data\":{\"feedId\":\"74858018884\",\"feedType\":\"POST_PRODUCT_PRICING_DATA\",\"marketplaceIds\":[\"ATVPDKIKX0DER\"],\"resultFeedDocumentId\":\"amzn1.tortuga.3.6812e402-d7d3-4b35-a2f9-e22ef13c92e1.T342OID692IDQB\"}}', '6195bf9c6c009', 1, 1, '', 1, 1, '202111181043383240145', 0, NULL);
INSERT INTO `admin_list` VALUES (2, 'rot/regist', '6195d02088d07', 0, 1, 1, '注册', '注册页面', 0, NULL, '6195bf9c6c009', 1, 1, '', 1, 1, '202111181202163951234', 0, NULL);
INSERT INTO `admin_list` VALUES (3, 'rot/regist2', '6195d0e197c8f', 0, 1, 1, '注册2', '注册页面', 0, NULL, '6195bf9c6c009', 1, 1, '', 1, 1, '202111181205171008626', 0, NULL);
INSERT INTO `admin_list` VALUES (4, '/user/getTestList', '619772571939e', 0, 1, 2, '请求test列表', '请求test列表', 0, NULL, 'default', 1, 1, '', 1, 1, '202111191749121674210', 0, NULL);

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `fid` int(11) NOT NULL DEFAULT 0 COMMENT '父级菜单ID',
  `url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '链接',
  `auth` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否需要登录才可以访问，1-需要，0-不需要',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `show` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否显示，1-显示，0-隐藏',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `level` tinyint(4) NOT NULL DEFAULT 1 COMMENT '菜单层级，1-一级菜单，2-二级菜单，3-按钮',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '前端组件',
  `router` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '前端路由',
  `log` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否记录日志，1-记录，0-不记录',
  `permission` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否验证权限，1-鉴权，0-放行',
  `method` tinyint(4) NOT NULL DEFAULT 1 COMMENT '请求方式，1-GET, 2-POST, 3-PUT, 4-DELETE',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 128 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '目录信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES (1, '用户登录', 73, 'admin/Login/index', 0, 0, 0, '', 2, '', '', 0, 0, 2);
INSERT INTO `admin_menu` VALUES (2, '用户登出', 73, 'admin/Login/logout', 1, 0, 0, '', 2, '', '', 1, 0, 1);
INSERT INTO `admin_menu` VALUES (3, '系统管理', 0, '', 1, 1, 1, 'ios-build', 1, '', '/system', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (4, '菜单维护', 3, '', 1, 1, 1, 'md-menu', 2, 'system/menu', 'menu', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (5, '菜单状态修改', 4, 'admin/Menu/changeStatus', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (6, '新增菜单', 4, 'admin/Menu/add', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (7, '编辑菜单', 4, 'admin/Menu/edit', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (8, '菜单删除', 4, 'admin/Menu/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (9, '用户管理', 3, '', 1, 2, 1, 'ios-people', 2, 'system/user', 'user', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (10, '获取当前组的全部用户', 9, 'admin/User/getUsers', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (11, '用户状态修改', 9, 'admin/User/changeStatus', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (12, '新增用户', 9, 'admin/User/add', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (13, '用户编辑', 9, 'admin/User/edit', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (14, '用户删除', 9, 'admin/User/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (15, '权限管理', 3, '', 1, 3, 1, 'md-lock', 2, 'system/auth', 'auth', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (16, '权限组状态编辑', 15, 'admin/Auth/changeStatus', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (17, '从指定组中删除指定用户', 15, 'admin/Auth/delMember', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (18, '新增权限组', 15, 'admin/Auth/add', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (19, '权限组编辑', 15, 'admin/Auth/edit', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (20, '删除权限组', 15, 'admin/Auth/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (21, '获取全部已开放的可选组', 15, 'admin/Auth/getGroups', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (22, '获取组所有的权限列表', 15, 'admin/Auth/getRuleList', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (23, '应用接入', 0, '', 1, 2, 1, 'ios-appstore', 1, '', '/apps', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (24, '应用管理', 23, '', 1, 0, 1, 'md-list-box', 2, 'app/list', 'appsList', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (25, '应用状态编辑', 24, 'admin/App/changeStatus', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (26, '获取AppId,AppSecret,接口列表,应用接口权限细节', 24, 'admin/App/getAppInfo', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (27, '新增应用', 24, 'admin/App/add', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (28, '编辑应用', 24, 'admin/App/edit', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (29, '删除应用', 24, 'admin/App/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (30, '接口管理', 0, '', 1, 3, 1, 'ios-link', 1, '', '/interface', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (31, '接口维护', 30, '', 1, 0, 1, 'md-infinite', 2, 'interface/list', 'interfaceList', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (32, '接口状态编辑', 31, 'admin/InterfaceList/changeStatus', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (33, '获取接口唯一标识', 31, 'admin/InterfaceList/getHash', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (34, '添加接口', 31, 'admin/InterfaceList/add', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (35, '编辑接口', 31, 'admin/InterfaceList/edit', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (36, '删除接口', 31, 'admin/InterfaceList/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (37, '获取接口请求字段', 30, 'admin/Fields/request', 1, 0, 1, '', 3, 'interface/request', 'request/:hash', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (38, '获取接口返回字段', 30, 'admin/Fields/response', 1, 0, 1, '', 3, 'interface/response', 'response/:hash', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (39, '添加接口字段', 31, 'admin/Fields/add', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (40, '上传接口返回字段', 31, 'admin/Fields/upload', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (41, '编辑接口字段', 31, 'admin/Fields/edit', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (42, '删除接口字段', 31, 'admin/Fields/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (43, '接口分组', 30, '', 1, 1, 1, 'md-archive', 2, 'interface/group', 'interfaceGroup', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (44, '添加接口组', 43, 'admin/InterfaceGroup/add', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (45, '编辑接口组', 43, 'admin/InterfaceGroup/edit', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (46, '删除接口组', 43, 'admin/InterfaceGroup/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (47, '获取全部有效的接口组', 43, 'admin/InterfaceGroup/getAll', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (48, '接口组状态维护', 43, 'admin/InterfaceGroup/changeStatus', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (49, '应用分组', 23, '', 1, 1, 1, 'ios-archive', 2, 'app/group', 'appsGroup', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (50, '添加应用组', 49, 'admin/AppGroup/add', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (51, '编辑应用组', 49, 'admin/AppGroup/edit', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (52, '删除应用组', 49, 'admin/AppGroup/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (53, '获取全部可用应用组', 49, 'admin/App/getAll', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (54, '应用组状态编辑', 49, 'admin/AppGroup/changeStatus', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (55, '菜单列表', 4, 'admin/Menu/index', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (56, '用户列表', 9, 'admin/User/index', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (57, '权限列表', 15, 'admin/Auth/index', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (58, '应用列表', 24, 'admin/App/index', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (59, '应用分组列表', 49, 'admin/AppGroup/index', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (60, '接口列表', 31, 'admin/InterfaceList/index', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (61, '接口分组列表', 43, 'admin/InterfaceGroup/index', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (62, '日志管理', 3, '', 1, 4, 1, 'md-clipboard', 2, 'system/log', 'log', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (63, '获取操作日志列表', 62, 'admin/Log/index', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (64, '删除单条日志记录', 62, 'admin/Log/del', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (65, '刷新路由', 31, 'admin/InterfaceList/refresh', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (67, '文件上传', 73, 'admin/Index/upload', 1, 0, 0, '', 2, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (68, '更新个人信息', 73, 'admin/User/own', 1, 0, 0, '', 2, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (69, '刷新AppSecret', 24, 'admin/App/refreshAppSecret', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (70, '获取用户信息', 73, 'admin/Login/getUserInfo', 1, 0, 0, '', 2, '', '', 0, 1, 1);
INSERT INTO `admin_menu` VALUES (71, '编辑权限细节', 15, 'admin/Auth/editRule', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (72, '获取用户有权限的菜单', 73, 'admin/Login/getAccessMenu', 1, 0, 0, '', 2, '', '', 0, 0, 1);
INSERT INTO `admin_menu` VALUES (73, '系统支撑', 0, '', 0, 0, 0, 'logo-tux', 1, '', '', 0, 0, 1);
INSERT INTO `admin_menu` VALUES (76, '获取应用分组', 24, 'admin/InterfaceList/group', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (78, '创建接口', 31, 'admin/InterfaceList/createFunc', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (79, '接口列表', 74, '', 1, 0, 1, 'md-infinite', 2, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (80, '接口分组', 74, '', 1, 0, 1, '', 2, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (83, '测试', 79, '', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (111, '获取接口请求字段', 75, 'admin/Fields/request', 1, 0, 1, '', 3, 'saas/request', 'request/:hash', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (112, '获取接口返回字段', 75, '', 1, 0, 1, '', 3, 'saas/response', 'response/:hash', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (122, '数据字段编辑', 91, 'admin/CustomDataBase/editDesignColumn', 1, 0, 1, '', 3, '', '', 1, 1, 2);
INSERT INTO `admin_menu` VALUES (123, '接口分组资源路由', 31, 'admin/InterfaceList/get_routes', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (124, '获取应用app分组', 24, 'admin/AppGroup/getAll', 1, 0, 1, '', 3, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (125, '生成文档', 0, 'admin/InterfaceList/createDoc', 1, 0, 1, '', 1, '', '', 1, 1, 1);
INSERT INTO `admin_menu` VALUES (127, '接口文档', 30, '', 1, 0, 1, 'md-infinite', 2, 'interface/wiki', 'wiki', 1, 1, 1);

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户密码',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `create_ip` bigint(20) NOT NULL DEFAULT 0 COMMENT '注册IP',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '账号状态 0封号 1正常',
  `openid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '三方登录唯一ID',
  `app_hash` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '账号绑定的应用',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员认证信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES (1, 'root', 'root', '20ce4f0c5a2cf8c5fb39358610e17aac', 1607505017, 2130706433, 1612684853, 1, NULL, '601f587ac8afc,601f629ed557e,601faaadc9706,601fb00056c45,601fb29bf2745,601fb3049c737,6063de3a8d821,607642fedb3be,60828b3035a58,6084cad1a01e7,6088beb8d8bf4,71163934,6088c7057a78a,607642f2b7084,608a8d870c870,608f7196b66e4,60a215bb81be9,60a20e3d329f9,60b84ec8e7297,60c185d1a9754,60c185d1a9754,60c185d1a9754,60e3f65a81314,60fa35199ae76,6180ac38c4447,6180ad145977b,6180adc2670d9,6180b0bc4c2c1,6180b1cc615f7,,,,,6182431139d36,6182431139d36,61825ef7d01e0,61825ef7d01e0,61825ef7d01e0,6182617a0e40a,6184eff67cbff,6195b7acbb478,6195cfb1cc8db,6195cfb8824ad');
INSERT INTO `admin_user` VALUES (2, 'admin', 'admin', 'f161fa801671c5211703a7713f89882a', 1607506345, 1001346446, 1607506345, 1, '', NULL);


-- ----------------------------
-- Table structure for admin_user_action
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_action`;
CREATE TABLE `admin_user_action`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行为名称',
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '操作用户ID',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '用户提交的数据',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '操作URL',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 939 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户操作日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_user_action
-- ----------------------------
INSERT INTO `admin_user_action` VALUES (1, '接口列表', 1, 'root', '{\"page\":\"1\",\"size\":\"40\",\"type\":\"\",\"keywords\":\"\",\"status\":\"\",\"group_hash\":\"\",\"app_group_id\":\"\"}', 'admin/InterfaceList/index', '2021-11-17 17:17:34');

-- ----------------------------
-- Table structure for admin_user_data
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_data`;
CREATE TABLE `admin_user_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_times` int(11) NOT NULL DEFAULT 0 COMMENT '账号登录次数',
  `last_login_ip` bigint(20) NOT NULL DEFAULT 0 COMMENT '最后登录IP',
  `last_login_time` int(11) NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `head_img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '用户头像',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin_user_data
-- ----------------------------
INSERT INTO `admin_user_data` VALUES (1, 16, 1989223598, 1637307738, 1, '');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for y_address_sheng
-- ----------------------------
DROP TABLE IF EXISTS `y_address_sheng`;
CREATE TABLE `y_address_sheng`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` int(11) NULL DEFAULT 0 COMMENT '编码',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '名称',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_time` datetime NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '省份表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_address_sheng
-- ----------------------------

-- ----------------------------
-- Table structure for y_address_shi
-- ----------------------------
DROP TABLE IF EXISTS `y_address_shi`;
CREATE TABLE `y_address_shi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL DEFAULT 0 COMMENT '编码',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `pcode` int(11) NOT NULL DEFAULT 0 COMMENT '上级code',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `delete_time` datetime NULL DEFAULT NULL COMMENT '删除时间',
  `is_on` tinyint(1) NULL DEFAULT 0 COMMENT '0未开通 1已开通',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `code`(`code`, `pcode`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '城市表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_address_shi
-- ----------------------------

-- ----------------------------
-- Table structure for y_address_xian
-- ----------------------------
DROP TABLE IF EXISTS `y_address_xian`;
CREATE TABLE `y_address_xian`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL DEFAULT 0,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pcode` int(11) NOT NULL DEFAULT 0,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `code`(`code`, `pcode`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '区县表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_address_xian
-- ----------------------------

-- ----------------------------
-- Table structure for y_admin
-- ----------------------------
DROP TABLE IF EXISTS `y_admin`;
CREATE TABLE `y_admin`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `de_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `account` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录账号',
  `realname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '真实姓名',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '邮箱',
  `gender` tinyint(1) NULL DEFAULT 0 COMMENT '性别 1男 2女 0未知',
  `mobile` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1正常 2禁用',
  `power` tinyint(1) NULL DEFAULT 0 COMMENT '权限 1超级管理员，2普通管理员【废弃】，3商家【代理商】，4运营推广【废弃】\r\n5编辑【废弃】 7多门店商家【废弃】 8多门店店长【废弃】 11配送员【废弃】 12客服',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `delete_time` datetime NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `accountpassword`(`account`, `password`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_admin
-- ----------------------------
INSERT INTO `y_admin` VALUES (41, NULL, 'kissneck', 'kissneck', 'd6eaba61b1b108baedef8032e9d77a40', '', '', 0, '', 1, 0, '2021-09-09 17:46:30', NULL, NULL);

-- ----------------------------
-- Table structure for y_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `y_admin_role`;
CREATE TABLE `y_admin_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `角色ID_copy_1`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_admin_role
-- ----------------------------

-- ----------------------------
-- Table structure for y_admin_user_action
-- ----------------------------
DROP TABLE IF EXISTS `y_admin_user_action`;
CREATE TABLE `y_admin_user_action`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '行为名称',
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '操作用户ID',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户昵称',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `data` json NULL COMMENT '用户提交的数据',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '操作URL',
  `type` tinyint(1) NULL DEFAULT NULL COMMENT '1 后台管理员操作日志  2用户操作日志',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户操作日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_admin_user_action
-- ----------------------------

-- ----------------------------
-- Table structure for y_department
-- ----------------------------
DROP TABLE IF EXISTS `y_department`;
CREATE TABLE `y_department`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `department_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门代码',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `department_num` int(11) NOT NULL DEFAULT 0 COMMENT '人数',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '上级部门,0表示没有上级',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序，越大权重越高',
  `assgin` int(11) NULL DEFAULT NULL COMMENT '部门负责人id',
  `update_time` datetime NULL DEFAULT NULL,
  `delete_time` datetime NULL DEFAULT NULL,
  `a_id` int(11) NULL DEFAULT NULL COMMENT '添加人id',
  `status` int(3) NOT NULL DEFAULT 1 COMMENT '状态 1启用 0禁用',
  `department_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_department
-- ----------------------------

-- ----------------------------
-- Table structure for y_menu
-- ----------------------------
DROP TABLE IF EXISTS `y_menu`;
CREATE TABLE `y_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `type` int(3) NULL DEFAULT NULL COMMENT '菜单类型 1，菜单  2，按钮',
  `menu_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `status` int(3) NOT NULL DEFAULT 1 COMMENT '状态 1，启用 2，禁用',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序，数字越大优先级越高',
  `pid` int(11) NULL DEFAULT NULL COMMENT '上级id',
  `power_mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识（路由标识）',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `menu_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限代码，自动生成',
  `menu_route` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后端路由',
  `front_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径（前端路由)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_menu
-- ----------------------------

-- ----------------------------
-- Table structure for y_role
-- ----------------------------
DROP TABLE IF EXISTS `y_role`;
CREATE TABLE `y_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限名称',
  `is_sys` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否系统内置(系统内置1不能删除)',
  `sort` int(11) NOT NULL DEFAULT 100 COMMENT '排序 数字越大权限越高',
  `role_describ` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限说明',
  `a_id` int(11) NULL DEFAULT NULL COMMENT '设置该角色的管理员ID',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `role_power` json NULL COMMENT '权限id,英文逗号隔开',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态 1启用 0禁用',
  `group_id` int(3) NULL DEFAULT NULL COMMENT '角色组id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_role
-- ----------------------------

-- ----------------------------
-- Table structure for y_role_group
-- ----------------------------
DROP TABLE IF EXISTS `y_role_group`;
CREATE TABLE `y_role_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色组名称',
  `group_describ` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色组描述',
  `status` tinyint(3) NOT NULL DEFAULT 1 COMMENT '状态 1，启用 0，禁用',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `group_power` json NULL COMMENT '权限 暂定菜单id,用英文逗号隔开',
  `delete_time` datetime NULL DEFAULT NULL,
  `a_id` int(11) NOT NULL DEFAULT 0 COMMENT '创建角色组管理员id，为0时系统添加',
  `is_sys` int(3) NOT NULL DEFAULT 0 COMMENT '是否系统内置 1，是（无法删除） 0，否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_role_group
-- ----------------------------

-- ----------------------------
-- Table structure for y_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `y_role_menu`;
CREATE TABLE `y_role_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `menu_id` int(11) NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `菜单ID`(`menu_id`) USING BTREE,
  INDEX `角色ID`(`role_id`) USING BTREE,
  CONSTRAINT `菜单ID` FOREIGN KEY (`menu_id`) REFERENCES `y_menu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `角色ID` FOREIGN KEY (`role_id`) REFERENCES `y_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for y_user
-- ----------------------------
DROP TABLE IF EXISTS `y_user`;
CREATE TABLE `y_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `job_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工号',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名(昵称)',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色id',
  `department_id` int(11) NULL DEFAULT NULL COMMENT '部门id',
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工手机号',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工邮箱',
  `status` int(3) NOT NULL DEFAULT 1 COMMENT '员工状态1正常，0不正常',
  `sex` int(3) NOT NULL DEFAULT 1 COMMENT '性别 1男  0女',
  `age` date NULL DEFAULT NULL COMMENT '年龄',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户备注',
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_power` json NULL COMMENT '用户权限',
  `user_type` int(3) NOT NULL DEFAULT 0 COMMENT '1为超级管理（无法修改） 0普通用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of y_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
